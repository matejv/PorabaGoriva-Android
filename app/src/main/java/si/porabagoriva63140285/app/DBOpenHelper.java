package si.porabagoriva63140285.app;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public class DBOpenHelper extends SQLiteOpenHelper {
    public DBOpenHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Vozilo(" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                "znamka TEXT," +
                "model TEXT," +
                "gorivo INT)");
        // gorivo: 1 - bencin, 2 - dizel

        db.execSQL("CREATE TABLE Razdalje(" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                "id_vozila INT," +
                "stanje_stevca INT," +
                "datum TEXT," +
                "FOREIGN KEY(id_vozila) REFERENCES Vozilo(_id))");

        db.execSQL("CREATE TABLE Polnjenje(" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                "id_vozila INT," +
                "cena_liter REAL," +
                "znesek REAL," +
                "litrov REAL," +
                "FOREIGN KEY(id_vozila) REFERENCES Vozilo(_id))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
