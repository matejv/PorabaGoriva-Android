package si.porabagoriva63140285.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DodajUrediPolnjenje extends Activity {
    private long row_id;
    private int stGoriva;
    private float kolicina;
    private TextView znamka, model, gorivo, litrov;
    private EditText datum, stevec, znesek, cenaLiter;

    private final Calendar koledar = Calendar.getInstance();
    private final DatePickerDialog.OnDateSetListener trenutniDatum = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            koledar.set(Calendar.YEAR, year);
            koledar.set(Calendar.MONTH, monthOfYear);
            koledar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            nastaviDatum();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_uredi_polnjenje);

        znamka = (TextView) findViewById(R.id.TV_Pznamka);
        model = (TextView) findViewById(R.id.TV_Pmodel);
        gorivo = (TextView) findViewById(R.id.TV_Pgorivo);
        litrov = (TextView) findViewById(R.id.TV_litrov);
        datum = (EditText) findViewById(R.id.ET_datum);
        stevec = (EditText) findViewById(R.id.ET_stevec);
        znesek = (EditText) findViewById(R.id.ET_znesek);
        cenaLiter = (EditText) findViewById(R.id.ET_cenaLiter);

        Bundle extras = getIntent().getExtras();
        row_id = extras.getLong("row_id");
        stGoriva = extras.getInt("stGoriva");

        if (getIntent().hasExtra("datum")) {
            this.setTitle("Uredi polnjenje");
            datum.setText(extras.getString("datum"));
            stevec.setText(Integer.toString(extras.getInt("stevec")));
            znesek.setText(extras.getString("znesek"));
            cenaLiter.setText(extras.getString("cenaLiter"));
            izracunajLitre();
        }
        else {
            new PridobiCene(this, stGoriva).execute();
            new PridobiZadnjoRazdaljo().execute(row_id);
            nastaviDatum(); // predhodno nastavi trenutni datum v polje za vnos datuma
            litrov.setText(String.format("%.02f", 0.0));
        }

        znamka.setText(extras.getString("znamka"));
        model.setText(extras.getString("model"));
        gorivo.setText(extras.getString("gorivo"));

        datum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(DodajUrediPolnjenje.this, trenutniDatum, koledar.get(Calendar.YEAR),
                        koledar.get(Calendar.MONTH), koledar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        znesek.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                izracunajLitre();
            }
        });

        cenaLiter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                izracunajLitre();
            }
        });
    }

    private void nastaviDatum() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd. MM. yyyy");
        datum.setText(sdf.format(koledar.getTime()));
    }

    private boolean preveriZneske() {
        if (znesek.getText().length() == 0)
            return false;

        if (cenaLiter.getText().length() == 0)
            return false;

        return true;
    }

    private void izracunajLitre() {
        if (preveriZneske()) {
            float z = Float.parseFloat(znesek.getText().toString());
            float cl = Float.parseFloat(cenaLiter.getText().toString());
            kolicina = z / cl;

            litrov.setText(String.format("%.02f", kolicina));
        }
        else
            litrov.setText(String.format("%.02f", 0.0));
    }

    private boolean preveriVnos() {
        if (stevec.getText().length() == 0)
            return false;

        if (znesek.getText().length() == 0)
            return false;

        if (cenaLiter.getText().length() == 0)
            return false;

        return true;
    }

    public void dodajUrediPolnjenje(View view) {
        if (preveriVnos()) {
            AsyncTask<Object, Object, Object> shraniPolnjenjeTask = new AsyncTask<Object, Object, Object>() {
                @Override
                protected Object doInBackground(Object... params) {
                    shraniPolnjenje();
                    return null;
                }

                @Override
                protected void onPostExecute(Object result) {
                    Toast.makeText(DodajUrediPolnjenje.this, "Uspešen vnos", Toast.LENGTH_SHORT).show();
                    finish();
                }
            };

            shraniPolnjenjeTask.execute();
        }
        else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("Neveljaven vnos");
            dialog.setMessage("Izpolniti je treba vsa vnosna polja.");
            dialog.setPositiveButton("V redu", null);
            dialog.show();
        }
    }

    private void shraniPolnjenje() {
        DBConnector dbConnector = new DBConnector(this);

        if (!getIntent().hasExtra("datum"))
        dbConnector.dodajPolnjenje(row_id, Integer.parseInt(stevec.getText().toString()),
                datum.getText().toString(), Float.parseFloat(cenaLiter.getText().toString()),
                Float.parseFloat(znesek.getText().toString()), kolicina);
        else
            dbConnector.spremeniPolnjenje(row_id, Integer.parseInt(stevec.getText().toString()),
                    datum.getText().toString(), Float.parseFloat(cenaLiter.getText().toString()),
                    Float.parseFloat(znesek.getText().toString()), kolicina);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dodaj_uredi_polnjenje, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private class PridobiZadnjoRazdaljo extends AsyncTask<Long, Object, Cursor> {
        DBConnector dbConnector = new DBConnector(DodajUrediPolnjenje.this);

        @Override
        protected Cursor doInBackground(Long... params) {
            dbConnector.open();
            return dbConnector.vseRazdalje(params[0]);
        }

        @Override
        protected void onPostExecute(Cursor result) {
            if (result.getCount() > 0) {
                result.moveToLast();
                stevec.setText(Integer.toString(result.getInt(result.getColumnIndex("stanje_stevca"))));
            }

            dbConnector.close();
        }
    }
}
