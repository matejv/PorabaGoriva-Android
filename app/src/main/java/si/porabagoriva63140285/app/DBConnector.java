package si.porabagoriva63140285.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBConnector {
    private static final String DB_NAME = "Poraba";
    private static final int DB_VERSION = 1;

    private SQLiteDatabase db;
    private DBOpenHelper dbOpenHelper;

    public DBConnector(Context context) {
        dbOpenHelper = new DBOpenHelper(context, DB_NAME, null, DB_VERSION);
    }

    public void open() {
        db = dbOpenHelper.getWritableDatabase();
    }

    public void close() {
        if (db != null)
            db.close();
    }

    public void dodajVozilo(String znamka, String model, int gorivo) {
        final ContentValues novoVozilo = new ContentValues();
        novoVozilo.put("znamka", znamka);
        novoVozilo.put("model", model);
        novoVozilo.put("gorivo", gorivo);

        open();
        db.insert("Vozilo", null, novoVozilo);
        close();
    }

    public void spremeniVozilo(long id, String znamka, String model, int gorivo) {
        final ContentValues spremembaVozila = new ContentValues();
        spremembaVozila.put("znamka", znamka);
        spremembaVozila.put("model", model);
        spremembaVozila.put("gorivo", gorivo);

        open();
        db.update("Vozilo", spremembaVozila, "_id=" + id, null);
        close();
    }

    // izbriše vozilo in vso zgodovino vozila
    public void izbrisiVozilo(long id) {
        open();
        db.delete("Polnjenje", "id_vozila=" + id, null);
        db.delete("Razdalje", "id_vozila=" + id, null);
        db.delete("Vozilo", "_id=" + id, null);
        close();
    }

    public Cursor vsaVozila() {
        return db.query("Vozilo", new String[] {"_id", "znamka", "model", "gorivo"}, null, null, null, null, null);
    }

    public Cursor enoVozilo(long id) {
        return db.query("Vozilo", null, "_id=" + id, null, null, null, null);
    }

    public void dodajPolnjenje(long id_vozila, int stanje_stevca, String datum,
                               float cena_liter, float znesek, float litrov) {
        final ContentValues novaRazdalja = new ContentValues();
        novaRazdalja.put("id_vozila", id_vozila);
        novaRazdalja.put("stanje_stevca", stanje_stevca);
        novaRazdalja.put("datum", datum);

        final ContentValues novoPolnjenje = new ContentValues();
        novoPolnjenje.put("id_vozila", id_vozila);
        novoPolnjenje.put("cena_liter", cena_liter);
        novoPolnjenje.put("znesek", znesek);
        novoPolnjenje.put("litrov", litrov);

        open();
        db.insert("Razdalje", null, novaRazdalja);
        db.insert("Polnjenje", null, novoPolnjenje);
        close();
    }

    public void spremeniPolnjenje(long id, int stanje_stevca, String datum, float cena_liter, float znesek, float litrov) {
        final ContentValues spremembaRazdalje = new ContentValues();
        spremembaRazdalje.put("stanje_stevca", stanje_stevca);
        spremembaRazdalje.put("datum", datum);

        final ContentValues spremembaPolnjenja = new ContentValues();
        spremembaPolnjenja.put("cena_liter", cena_liter);
        spremembaPolnjenja.put("znesek", znesek);
        spremembaPolnjenja.put("litrov", litrov);

        open();
        db.update("Razdalje", spremembaRazdalje, "_id=" + id, null);
        db.update("Polnjenje", spremembaPolnjenja, "_id=" + id, null);
        close();
    }

    public void izbrisiPolnjenje(long id) {
        open();
        db.delete("Polnjenje", "_id=" + id, null);
        db.delete("Razdalje", "_id=" + id, null);
        close();
    }

    public Cursor vsaPolnjenja(long id_vozila) {
        return db.query("Polnjenje", new String[] {"_id", "litrov", "cena_liter", "znesek"},
                "id_vozila=" + id_vozila, null, null, null, "_id DESC");
    }

    public Cursor vseRazdalje(long id_vozila) {
        return db.query("Razdalje", new String[] {"_id", "stanje_stevca", "datum"},
                "id_vozila=" + id_vozila, null, null, null, null);
    }

    public Cursor enoPolnjenje(long id) {
        return db.query("Polnjenje", null, "_id=" + id, null, null, null, null);
    }

    public Cursor enaRazdalja(long id) {
        return db.query("Razdalje", null, "_id=" + id, null, null, null, null);
    }
}
