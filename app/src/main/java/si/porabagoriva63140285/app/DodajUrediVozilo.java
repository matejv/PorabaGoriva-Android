package si.porabagoriva63140285.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class DodajUrediVozilo extends Activity {
    private long row_id;
    private EditText znamka, model;
    private RadioButton bencin, dizel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_uredi_vozilo);

        znamka = (EditText) findViewById(R.id.ET_znamka);
        model = (EditText) findViewById(R.id.ET_model);
        bencin = (RadioButton) findViewById(R.id.RB_bencin);
        dizel = (RadioButton) findViewById(R.id.RB_dizel);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            this.setTitle("Uredi vozilo");
            row_id = extras.getLong("row_id");
            znamka.setText(extras.getString("znamka"));
            model.setText(extras.getString("model"));
            int gorivo = extras.getInt("gorivo");

            if (gorivo == 1)
                bencin.setChecked(true);
            else
                dizel.setChecked(true);
        }
    }

    private boolean preveriVnos() {
        if (znamka.getText().length() == 0)
            return false;

        if (model.getText().length() == 0)
            return false;

        if (!bencin.isChecked() && !dizel.isChecked())
            return false;

        return true;
    }

    public void dodajUrediVozilo(View view) {
        if (preveriVnos()) {
            AsyncTask<Object, Object, Object> shraniVoziloTask = new AsyncTask<Object, Object, Object>() {
                @Override
                protected Object doInBackground(Object... params) {
                    shraniVozilo();
                    return null;
                }

                @Override
                protected void onPostExecute(Object result) {
                    Toast.makeText(DodajUrediVozilo.this, "Uspešen vnos", Toast.LENGTH_SHORT).show();
                    finish();
                }
            };

            shraniVoziloTask.execute();
        }
        else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("Neveljaven vnos");
            dialog.setMessage("Izpolniti je treba vsa vnosna polja.");
            dialog.setPositiveButton("V redu", null);
            dialog.show();
        }
    }

    private void shraniVozilo() {
        DBConnector dbConnector = new DBConnector(this);

        if (getIntent().getExtras() == null)
            dbConnector.dodajVozilo(znamka.getText().toString(), model.getText().toString(),
                    (bencin.isChecked()) ? 1 : 2);
        else
            dbConnector.spremeniVozilo(row_id, znamka.getText().toString(), model.getText().toString(),
                    (bencin.isChecked()) ? 1 : 2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dodaj_uredi_vozilo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }
}
