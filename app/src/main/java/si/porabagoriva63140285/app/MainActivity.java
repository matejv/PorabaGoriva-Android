package si.porabagoriva63140285.app;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

public class MainActivity extends Activity {
    private ListView vsaVozila;
    private SimpleCursorAdapter cursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vsaVozila = (ListView) findViewById(R.id.LV_VsaVozila);
        vsaVozila.setOnItemClickListener(prikaziVozilo);

        String[] from = new String[] {"_id", "znamka", "model", "gorivo"};
        int[] to = new int[] {R.id.VLI_id, R.id.VLI_znamka, R.id.VLI_model, R.id.VLI_gorivo};
        cursorAdapter = new SimpleCursorAdapter(this, R.layout.vozilo_list_item, null, from, to, 0);

        cursorAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                if (view.getId() == R.id.VLI_gorivo) {
                    int g = cursor.getInt(cursor.getColumnIndex("gorivo"));
                    ((TextView) view).setText((g == 1) ? "(bencin)" : "(dizel)");

                    return true;
                }

                return false;
            }
        } );

        vsaVozila.setAdapter(cursorAdapter);
    }

    private class PridobiVozila extends AsyncTask<Object, Object, Cursor> {
        DBConnector dbConnector = new DBConnector(MainActivity.this);

        @Override
        protected Cursor doInBackground(Object... params) {
            dbConnector.open();
            return dbConnector.vsaVozila();
        }

        @Override
        protected void onPostExecute(Cursor result) {
            cursorAdapter.changeCursor(result);
            dbConnector.close();
        }
    }

    AdapterView.OnItemClickListener prikaziVozilo = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent podrobnostiVozila = new Intent(MainActivity.this, PoglejVozilo.class);
            podrobnostiVozila.putExtra("row_id", id);
            startActivity(podrobnostiVozila);
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        new PridobiVozila().execute();
    }

    @Override
    public void onStop() {
        cursorAdapter.changeCursor(null);
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_dodajVozilo) {
            Intent intent = new Intent(this, DodajUrediVozilo.class);
            startActivity(intent);

            return true;
        }
        else if (id == R.id.action_cenaGoriva) {
            Intent intent = new Intent(this, CeneGoriv.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
