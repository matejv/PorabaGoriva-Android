package si.porabagoriva63140285.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class PoglejPolnjenje extends Activity {
    private long row_id;
    private int stGoriva;
    private TextView znamka, model, gorivo, datum, stevec, znesek, cenaLiter, litrov;
    private float z, cl, l;
    private String d;
    private int s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poglej_polnjenje);

        Bundle extras = getIntent().getExtras();
        row_id = extras.getLong("row_id");
        stGoriva = extras.getInt("stGoriva");

        znamka = (TextView) findViewById(R.id.TV_Pznamka);
        model = (TextView) findViewById(R.id.TV_Pmodel);
        gorivo = (TextView) findViewById(R.id.TV_Pgorivo);
        datum = (TextView) findViewById(R.id.TV_Pdatum);
        stevec = (TextView) findViewById(R.id.TV_Pstevec);
        znesek = (TextView) findViewById(R.id.TV_Pznesek);
        cenaLiter = (TextView) findViewById(R.id.TV_PcenaLiter);
        litrov = (TextView) findViewById(R.id.TV_Plitrov);

        znamka.setText(extras.getString("znamka"));
        model.setText(extras.getString("model"));
        gorivo.setText(extras.getString("gorivo"));
    }

    @Override
    public void onResume() {
        super.onResume();

        new PridobiRazdaljo().execute(row_id);
        new PridobiPolnjenje().execute(row_id);
    }

    public void pojdiNazaj(View view) {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_poglej_polnjenje, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_urediPolnjenje) {
            Intent intent = new Intent(this, DodajUrediPolnjenje.class);
            intent.putExtra("row_id", row_id);
            intent.putExtra("stGoriva", stGoriva);
            intent.putExtra("znamka", znamka.getText());
            intent.putExtra("model", model.getText());
            intent.putExtra("gorivo", gorivo.getText());
            intent.putExtra("datum", d);
            intent.putExtra("stevec", s);
            intent.putExtra("znesek", Float.toString(z));
            intent.putExtra("cenaLiter", Float.toString(cl));
            startActivity(intent);

            return true;
        }
        else if (id == R.id.action_izbrisiPolnjenje) {
            izbrisiPolnjenje();
            return true;
        }
        else if (id == R.id.action_cenaGoriva) {
            Intent intent = new Intent(this, CeneGoriv.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void izbrisiPolnjenje() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Potrdi brisanje");
        dialog.setMessage("Ali ste prepričani, da želite izbrisati to polnjenje?");
        dialog.setNegativeButton("Prekliči", null);
        dialog.setPositiveButton("V redu", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final DBConnector dbConnector = new DBConnector(PoglejPolnjenje.this);

                AsyncTask<Long, Object, Object> brisanjePolnjenjaTask = new AsyncTask<Long, Object, Object>() {
                    @Override
                    protected Object doInBackground(Long... params) {
                        dbConnector.izbrisiPolnjenje(params[0]);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Object result) {
                        Toast.makeText(PoglejPolnjenje.this, "Polnjenje izbrisano", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                };

                brisanjePolnjenjaTask.execute(row_id);
            }
        });

        dialog.show();
    }

    private class PridobiPolnjenje extends AsyncTask<Long, Object, Cursor> {
        DBConnector dbConnector = new DBConnector(PoglejPolnjenje.this);

        @Override
        protected Cursor doInBackground(Long... params) {
            dbConnector.open();
            return dbConnector.enoPolnjenje(params[0]);
        }

        @Override
        protected void onPostExecute(Cursor result) {
            result.moveToFirst();

            int cenaLiterIdx = result.getColumnIndex("cena_liter");
            int znesekIdx = result.getColumnIndex("znesek");
            int litrovIdx = result.getColumnIndex("litrov");

            cl = result.getFloat(cenaLiterIdx);
            z = result.getFloat(znesekIdx);
            l = result.getFloat(litrovIdx);

            cenaLiter.setText(String.format("%.03f", cl));
            znesek.setText(String.format("%.02f", z));
            litrov.setText(String.format("%.02f", l));

            result.close();
            dbConnector.close();
        }
    }

    private class PridobiRazdaljo extends AsyncTask<Long, Object, Cursor> {
        DBConnector dbConnector = new DBConnector(PoglejPolnjenje.this);

        @Override
        protected Cursor doInBackground(Long... params) {
            dbConnector.open();
            return dbConnector.enaRazdalja(params[0]);
        }

        @Override
        protected void onPostExecute(Cursor result) {
            result.moveToFirst();

            int stevecIdx = result.getColumnIndex("stanje_stevca");
            int datumIdx = result.getColumnIndex("datum");

            s = result.getInt(stevecIdx);
            d = result.getString(datumIdx);

            stevec.setText(Integer.toString(s));
            datum.setText(d);

            result.close();
            dbConnector.close();
        }
    }
}
