package si.porabagoriva63140285.app;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.EditText;
import android.widget.TextView;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class PridobiCene extends AsyncTask<String, Object, InputStream> {
    private static final String naslov = "http://www.petrol.eu/api/fuel_prices.xml";
    private static float[] cene = new float[3];
    private Activity activity;
    private int vrstaGoriva;

    public PridobiCene(Activity activity, int vrstaGoriva) {
        this.activity = activity;
        this.vrstaGoriva = vrstaGoriva;
    }

    @Override
    protected InputStream doInBackground(String... params) {
        try {
            URL url = new URL(naslov);
            URLConnection conn = url.openConnection();

            return conn.getInputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(InputStream result) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(result);

            NodeList countries = doc.getElementsByTagName("country");

            for (int i = 0; i < countries.getLength(); i++) {
                Element country = (Element) countries.item(i);
                NodeList fuels = country.getElementsByTagName("fuel");
                NamedNodeMap attrCountry = countries.item(i).getAttributes();

                if (attrCountry != null) {
                    Node countryNode = attrCountry.getNamedItem("label");

                    if (countryNode.getTextContent().equals("Slovenia")) {
                        for (int j = 0; j < fuels.getLength(); j++) {
                            Element fuel = (Element) fuels.item(j);
                            NodeList priceTypes = fuel.getElementsByTagName("priceType");
                            NamedNodeMap attrFuel = fuels.item(j).getAttributes();

                            if (attrFuel != null) {
                                Node fuelNode = attrFuel.getNamedItem("type");

                                if (fuelNode.getTextContent().equals("95") ||
                                        fuelNode.getTextContent().equals("100") ||
                                        fuelNode.getTextContent().equals("diesel")) {

                                    for (int k = 0; k < priceTypes.getLength(); k++) {
                                        Element priceType = (Element) priceTypes.item(k);
                                        NodeList prices = priceType.getElementsByTagName("price");

                                        for (int l = 0; l < prices.getLength(); l++) {
                                            NamedNodeMap attrPrice = prices.item(l).getAttributes();

                                            if (attrPrice != null) {
                                                Node priceNode = attrPrice.getNamedItem("type");

                                                if (priceNode.getTextContent().equals("price")) {
                                                    float price = Float.parseFloat(prices.item(l).getTextContent());

                                                    switch (fuelNode.getTextContent()) {
                                                        case "95":
                                                            cene[0] = price;
                                                            break;
                                                        case "100":
                                                            cene[1] = price;
                                                            break;
                                                        case "diesel":
                                                            cene[2] = price;
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (vrstaGoriva == 0) {
            TextView bencin95 = (TextView) activity.findViewById(R.id.TV_CG95);
            TextView bencin100 = (TextView) activity.findViewById(R.id.TV_CG100);
            TextView dizel = (TextView) activity.findViewById(R.id.TV_CGdizel);

            bencin95.setText(String.format("%.03f €/l", cene[0]));
            bencin100.setText(String.format("%.03f €/l", cene[1]));
            dizel.setText(String.format("%.03f €/l", cene[2]));
        }
        else if (vrstaGoriva == 1 || vrstaGoriva == 2) {
            EditText cenaLiter = (EditText) activity.findViewById(R.id.ET_cenaLiter);
            cenaLiter.setText((vrstaGoriva == 1) ? Float.toString(cene[0]) : Float.toString(cene[2]));
        }
    }
}
