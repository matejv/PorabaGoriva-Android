package si.porabagoriva63140285.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

public class PoglejVozilo extends Activity {
    private long row_id;
    private int stGoriva;
    private TextView znamka, model, gorivo, povpPoraba;
    private ListView vsaPolnjenja;
    private SimpleCursorAdapter cursorAdapter;
    private double sumLitri;
    private int sumKilometri, lock; // lock, če je 2 sta se AsynTaska zaključila, lahko zračunaš povprečno porabo

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poglej_vozilo);

        Bundle extras = getIntent().getExtras();
        row_id = extras.getLong("row_id");

        znamka = (TextView) findViewById(R.id.TV_znamka);
        model = (TextView) findViewById(R.id.TV_model);
        gorivo = (TextView) findViewById(R.id.TV_gorivo);
        povpPoraba = (TextView) findViewById(R.id.TV_povpPoraba);
        vsaPolnjenja = (ListView) findViewById(R.id.LV_Polnjenja);

        povpPoraba.setText(String.format("%.02f", 0.0));
        vsaPolnjenja.setOnItemClickListener(prikaziPolnjenje);

        String[] from = new String[] {"litrov", "cena_liter", "znesek"};
        int[] to = new int[] {R.id.PLI_litrov, R.id.PLI_cenaLiter, R.id.PLI_znesek};
        cursorAdapter = new SimpleCursorAdapter(this, R.layout.polnjenje_list_item, null, from, to, 0);

        cursorAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                if (view.getId() == R.id.PLI_litrov) {
                    float l = cursor.getFloat(cursor.getColumnIndex("litrov"));
                    ((TextView) view).setText(String.format("%.02f", l) + " l");
                    return true;
                }

                if (view.getId() == R.id.PLI_cenaLiter) {
                    float cl = cursor.getFloat(cursor.getColumnIndex("cena_liter"));
                    ((TextView) view).setText(String.format("%.03f", cl) + " €");
                    return true;
                }

                if (view.getId() == R.id.PLI_znesek) {
                    float z = cursor.getFloat(cursor.getColumnIndex("znesek"));
                    ((TextView) view).setText(String.format("%.02f", z) + " €");
                    return true;
                }

                return false;
            }
        });

        vsaPolnjenja.setAdapter(cursorAdapter);
    }

    AdapterView.OnItemClickListener prikaziPolnjenje = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(PoglejVozilo.this, PoglejPolnjenje.class);
            intent.putExtra("row_id", cursorAdapter.getItemId(position));
            intent.putExtra("stGoriva", stGoriva);
            intent.putExtra("znamka", znamka.getText());
            intent.putExtra("model", model.getText());
            intent.putExtra("gorivo", gorivo.getText());
            startActivity(intent);
        }
    };

    public void odpriNovoPolnjenje(View view) {
        Intent intent = new Intent(this, DodajUrediPolnjenje.class);
        intent.putExtra("row_id", row_id);
        intent.putExtra("stGoriva", stGoriva);
        intent.putExtra("znamka", znamka.getText());
        intent.putExtra("model", model.getText());
        intent.putExtra("gorivo", gorivo.getText());
        startActivity(intent);
    }

    private void izracunajPovpPorabo() {
        if (sumKilometri > 0) {
            double p = (sumLitri / sumKilometri) * 100;
            povpPoraba.setText(String.format("%.02f", p));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        sumLitri = 0.0;
        sumKilometri = 0;
        lock = 0;
        new PodatkiVozila().execute(row_id);
        new PridobiPolnjenja().execute(row_id);
        new PridobiKilometrino().execute(row_id);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_poglej_vozilo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_urediVozilo) {
            Intent intent = new Intent(this, DodajUrediVozilo.class);
            intent.putExtra("row_id", row_id);
            intent.putExtra("znamka", znamka.getText());
            intent.putExtra("model", model.getText());
            intent.putExtra("gorivo", stGoriva);
            startActivity(intent);

            return true;
        }
        else if (id == R.id.action_izbrisiVozilo) {
            izbrisiVozilo();
            return true;
        }
        else if (id == R.id.action_cenaGoriva) {
            Intent intent = new Intent(this, CeneGoriv.class);
            startActivity(intent);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void izbrisiVozilo() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Potrdi brisanje");
        dialog.setMessage("Ali ste prepričani, da želite izbrisati to vozilo in pripadajočo zgodovino polnjenj?");
        dialog.setNegativeButton("Prekliči", null);
        dialog.setPositiveButton("V redu", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final DBConnector dbConnector = new DBConnector(PoglejVozilo.this);

                AsyncTask<Long, Object, Object> brisanjeVozilaTask = new AsyncTask<Long, Object, Object>() {
                    @Override
                    protected Object doInBackground(Long... params) {
                        dbConnector.izbrisiVozilo(params[0]);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Object result) {
                        Toast.makeText(PoglejVozilo.this, "Vozilo izbrisano", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                };

                brisanjeVozilaTask.execute(row_id);
            }
        });

        dialog.show();
    }

    private class PodatkiVozila extends AsyncTask<Long, Object, Cursor> {
        DBConnector dbConnector = new DBConnector(PoglejVozilo.this);

        @Override
        protected Cursor doInBackground(Long... params) {
            dbConnector.open();
            return dbConnector.enoVozilo(params[0]);
        }

        @Override
        protected void onPostExecute(Cursor result) {
            result.moveToFirst();

            int znamkaIdx = result.getColumnIndex("znamka");
            int modelIdx = result.getColumnIndex("model");
            int gorivoIdx = result.getColumnIndex("gorivo");

            znamka.setText(result.getString(znamkaIdx));
            model.setText(result.getString(modelIdx));
            stGoriva = result.getInt(gorivoIdx);
            gorivo.setText((stGoriva == 1) ? "(bencin)" : "(dizel)");

            result.close();
            dbConnector.close();
        }
    }

    private class PridobiPolnjenja extends AsyncTask<Long, Object, Cursor> {
        DBConnector dbConnector = new DBConnector(PoglejVozilo.this);

        @Override
        protected Cursor doInBackground(Long... params) {
            dbConnector.open();
            return dbConnector.vsaPolnjenja(params[0]);
        }

        @Override
        protected void onPostExecute(Cursor result) {
            if (result.getCount() > 0) {
                while (result.moveToNext()) {
                    sumLitri += result.getFloat(result.getColumnIndex("litrov"));
                }

                // odšteje zadnjo natočeno količino, ki še ni bila porabljena
                result.moveToLast();
                sumLitri -= result.getFloat(result.getColumnIndex("litrov"));

                if (++lock == 2)
                    izracunajPovpPorabo();
            }

            cursorAdapter.changeCursor(result);
            dbConnector.close();
        }
    }

    private class PridobiKilometrino extends AsyncTask<Long, Object, Cursor> {
        DBConnector dbConnector = new DBConnector(PoglejVozilo.this);

        @Override
        protected Cursor doInBackground(Long... params) {
            dbConnector.open();
            return dbConnector.vseRazdalje(params[0]);
        }

        @Override
        protected void onPostExecute(Cursor result) {
            if (result.getCount() > 0) {
                result.moveToLast();
                sumKilometri = result.getInt(result.getColumnIndex("stanje_stevca"));

                result.moveToFirst();
                sumKilometri -= result.getInt(result.getColumnIndex("stanje_stevca"));

                if (++lock == 2)
                    izracunajPovpPorabo();
            }

            dbConnector.close();
        }
    }
}
